package pl.com.tim.hlebik.pizzamanager.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.com.tim.hlebik.pizzamanager.assemblers.PizzaSizeAssembler;
import pl.com.tim.hlebik.pizzamanager.models.PizzaSize;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaSizeBean;
import pl.com.tim.hlebik.pizzamanager.services.PizzaSizeService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/pizzaSize")
public class PizzaSizeRest {
    private final PizzaSizeService pizzaSizeService;
    private final PizzaSizeAssembler pizzaSizeAssembler;

    public PizzaSizeRest(PizzaSizeService pizzaSizeService,
                         PizzaSizeAssembler pizzaSizeAssembler) {
        this.pizzaSizeService = pizzaSizeService;
        this.pizzaSizeAssembler = pizzaSizeAssembler;
    }

    @GetMapping("/all")
    public ResponseEntity<List<PizzaSizeBean>> getAllPizzaSizes() {
        List<PizzaSizeBean> pizzaSizeBeans = pizzaSizeService.getAll()
                .stream()
                .map(pizzaSizeAssembler::mapModelToBean)
                .collect(Collectors.toList());
        return ResponseEntity.ok(pizzaSizeBeans);
    }

    @PostMapping
    public ResponseEntity<PizzaSizeBean> createPizzaSize(@RequestBody PizzaSizeBean pizzaSizeBean) {
        PizzaSize createdPizzaSize = pizzaSizeService.create(pizzaSizeBean);
        PizzaSizeBean createdPizzaSizeBean = pizzaSizeAssembler.mapModelToBean(createdPizzaSize);
        return ResponseEntity.ok(createdPizzaSizeBean);
    }

    @PutMapping
    public ResponseEntity<PizzaSizeBean> updatePizzaSize(@RequestBody PizzaSizeBean pizzaSizeBean) {
        PizzaSize updatedPizzaSize = pizzaSizeService.update(pizzaSizeBean);
        PizzaSizeBean updatedPizzaSizeBean = pizzaSizeAssembler.mapModelToBean(updatedPizzaSize);
        return ResponseEntity.ok(updatedPizzaSizeBean);
    }

    @GetMapping("/{pizzaSizeId}")
    public ResponseEntity<PizzaSizeBean> getPizzaSizeById(@PathVariable Long pizzaSizeId) {
        PizzaSize pizzaSize = pizzaSizeService.getById(pizzaSizeId);
        PizzaSizeBean pizzaSizeBean = pizzaSizeAssembler.mapModelToBean(pizzaSize);
        return ResponseEntity.ok(pizzaSizeBean);
    }

    @DeleteMapping("/{pizzaSizeId}")
    public ResponseEntity<Long> deletePizzaSize(@PathVariable Long pizzaSizeId) {
        Long deletedId = pizzaSizeService.deleteById(pizzaSizeId);
        return ResponseEntity.ok(deletedId);
    }
}
