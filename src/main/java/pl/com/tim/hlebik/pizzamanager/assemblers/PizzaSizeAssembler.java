package pl.com.tim.hlebik.pizzamanager.assemblers;

import org.springframework.stereotype.Component;
import pl.com.tim.hlebik.pizzamanager.models.PizzaSize;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaSizeBean;

@Component
public class PizzaSizeAssembler {
    public PizzaSizeBean mapModelToBean(PizzaSize pizzaSize) {
        Long id = pizzaSize.getId();
        String name = pizzaSize.getName();
        Integer diameter = pizzaSize.getDiameter();
        return new PizzaSizeBean(id, name, diameter);
    }
}
