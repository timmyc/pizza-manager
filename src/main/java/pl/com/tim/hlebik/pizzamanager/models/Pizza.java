package pl.com.tim.hlebik.pizzamanager.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Pizza {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    @Size(min = 3, max = 64)
    private String name;
    @ManyToMany
    @JoinTable(name = "pizza_ingredient",
            joinColumns = {@JoinColumn(name = "ingredient_id")},
            inverseJoinColumns = {@JoinColumn(name = "pizza_id")})
    private Set<Ingredient> ingredients = new HashSet<>();
    @OneToMany(mappedBy = "pizza", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PizzaMenuEntry> pizzaMenuEntries = new ArrayList<>();

    public Pizza(String name, Set<Ingredient> ingredients) {
        this.name = name;
        this.ingredients = ingredients;
    }
}
