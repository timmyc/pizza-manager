package pl.com.tim.hlebik.pizzamanager.models.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PizzaBean {
    private Long id;
    private String name;
    private Set<IngredientBean> ingredients;
}
