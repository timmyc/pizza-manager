package pl.com.tim.hlebik.pizzamanager.models.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IngredientBean {
    private Long id;
    private String name;
}
