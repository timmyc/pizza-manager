package pl.com.tim.hlebik.pizzamanager.assemblers;

import org.springframework.stereotype.Component;
import pl.com.tim.hlebik.pizzamanager.models.Ingredient;
import pl.com.tim.hlebik.pizzamanager.models.beans.IngredientBean;

@Component
public class IngredientAssembler {

    public IngredientBean mapModelToBean(Ingredient ingredient) {
        Long id = ingredient.getId();
        String name = ingredient.getName();
        return new IngredientBean(id, name);
    }
}
