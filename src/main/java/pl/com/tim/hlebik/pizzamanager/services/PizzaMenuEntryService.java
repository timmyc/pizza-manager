package pl.com.tim.hlebik.pizzamanager.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.com.tim.hlebik.pizzamanager.exceptions.PizzaMenuEntryNotFoundException;
import pl.com.tim.hlebik.pizzamanager.models.Pizza;
import pl.com.tim.hlebik.pizzamanager.models.PizzaMenuEntry;
import pl.com.tim.hlebik.pizzamanager.models.PizzaSize;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaMenuEntryBean;
import pl.com.tim.hlebik.pizzamanager.repositories.PizzaMenuEntryRepository;

import java.util.List;

@Slf4j
@Service
public class PizzaMenuEntryService {
    private final PizzaMenuEntryRepository pizzaMenuEntryRepository;
    private final PizzaSizeService pizzaSizeService;
    private final PizzaService pizzaService;

    public PizzaMenuEntryService(PizzaMenuEntryRepository pizzaMenuEntryRepository,
                                 PizzaSizeService pizzaSizeService,
                                 PizzaService pizzaService) {
        this.pizzaMenuEntryRepository = pizzaMenuEntryRepository;
        this.pizzaSizeService = pizzaSizeService;
        this.pizzaService = pizzaService;
    }

    public PizzaMenuEntry create(PizzaMenuEntryBean pizzaMenuEntryBean) {
        Double price = pizzaMenuEntryBean.getPrice();
        Long sizeId = pizzaMenuEntryBean.getSize().getId();
        PizzaSize size = pizzaSizeService.getById(sizeId);
        Long pizzaId = pizzaMenuEntryBean.getPizza().getId();
        Pizza pizza = pizzaService.getById(pizzaId);
        PizzaMenuEntry pizzaMenuEntry = new PizzaMenuEntry(price, size, pizza);
        pizzaMenuEntryRepository.save(pizzaMenuEntry);
        log.debug("PizzaMenuEntry: " + pizzaMenuEntry.getId() + " created successfully");
        return pizzaMenuEntry;
    }

    public PizzaMenuEntry updatePrice(PizzaMenuEntryBean pizzaMenuEntryBean) {
        Long id = pizzaMenuEntryBean.getId();
        Double price = pizzaMenuEntryBean.getPrice();
        Long sizeId = pizzaMenuEntryBean.getSize().getId();
        PizzaSize size = pizzaSizeService.getById(sizeId);
        Long pizzaId = pizzaMenuEntryBean.getPizza().getId();
        Pizza pizza = pizzaService.getById(pizzaId);
        PizzaMenuEntry pizzaMenuEntry = getById(id);
        pizzaMenuEntry.setPrice(price);
        pizzaMenuEntry.setSize(size);
        pizzaMenuEntry.setPizza(pizza);
        pizzaMenuEntryRepository.save(pizzaMenuEntry);
        log.debug("Price of pizzaMenuEntry: " + pizzaMenuEntry.getId() + " updated successfully");
        return pizzaMenuEntry;
    }

    public PizzaMenuEntry getById(Long id) {
        return pizzaMenuEntryRepository.findById(id).orElseThrow(() -> new PizzaMenuEntryNotFoundException(id));
    }

    public List<PizzaMenuEntry> getAll() {
        return pizzaMenuEntryRepository.findAll();
    }

    public Long deleteById(Long id) {
        PizzaMenuEntry pizzaMenuEntry = getById(id);
        pizzaMenuEntryRepository.delete(pizzaMenuEntry);
        log.debug("PizzaMenuEntry with id: " + id + " deleted successfully");
        return id;
    }
}
