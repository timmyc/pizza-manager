package pl.com.tim.hlebik.pizzamanager.models.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PizzaMenuEntryBean {
    private Long id;
    private Double price;
    private PizzaSizeBean size;
    private PizzaBean pizza;
}
