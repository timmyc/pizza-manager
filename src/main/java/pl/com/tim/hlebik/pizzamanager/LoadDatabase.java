package pl.com.tim.hlebik.pizzamanager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.com.tim.hlebik.pizzamanager.models.Ingredient;
import pl.com.tim.hlebik.pizzamanager.models.Pizza;
import pl.com.tim.hlebik.pizzamanager.models.PizzaMenuEntry;
import pl.com.tim.hlebik.pizzamanager.models.PizzaSize;
import pl.com.tim.hlebik.pizzamanager.repositories.IngredientRepository;
import pl.com.tim.hlebik.pizzamanager.repositories.PizzaMenuEntryRepository;
import pl.com.tim.hlebik.pizzamanager.repositories.PizzaRepository;
import pl.com.tim.hlebik.pizzamanager.repositories.PizzaSizeRepository;

import java.util.Set;

@Slf4j
@Configuration
class LoadDatabase {

    public static final String PRELOADING = "Preloading ";

    @Bean
    CommandLineRunner initDatabase(PizzaRepository pizzaRepository,
                                   PizzaMenuEntryRepository pizzaMenuEntryRepository,
                                   IngredientRepository ingredientRepository,
                                   PizzaSizeRepository pizzaSizeRepository) {

        return args -> {

            Ingredient cheese = new Ingredient("cheese");
            log.info(PRELOADING + ingredientRepository.save(cheese));
            Ingredient ketchup = new Ingredient("ketchup");
            log.info(PRELOADING + ingredientRepository.save(ketchup));
            Ingredient salami = new Ingredient("salami");
            log.info(PRELOADING + ingredientRepository.save(salami));
            Ingredient chicken = new Ingredient("chicken");
            log.info(PRELOADING + ingredientRepository.save(chicken));
            Ingredient pineapple = new Ingredient("pineapple");
            log.info(PRELOADING + ingredientRepository.save(pineapple));

            PizzaSize m = new PizzaSize("M", 32);
            log.info(PRELOADING + pizzaSizeRepository.save(m));
            PizzaSize l = new PizzaSize("L", 42);
            log.info(PRELOADING + pizzaSizeRepository.save(l));
            PizzaSize xl = new PizzaSize("XL", 50);
            log.info(PRELOADING + pizzaSizeRepository.save(xl));


            Pizza margherita = new Pizza("margherita", Set.of(ketchup, cheese));
            log.info(PRELOADING + pizzaRepository.save(margherita));
            Pizza hawaii = new Pizza("hawaii", Set.of(ketchup, cheese, chicken, pineapple));
            log.info(PRELOADING + pizzaRepository.save(hawaii));


            PizzaMenuEntry hawaiiM = new PizzaMenuEntry(34.99, m, hawaii);
            log.info(PRELOADING + pizzaMenuEntryRepository.save(hawaiiM));
            PizzaMenuEntry hawaiiL = new PizzaMenuEntry(40.0, l, hawaii);
            log.info(PRELOADING + pizzaMenuEntryRepository.save(hawaiiL));
            PizzaMenuEntry hawaiiXL = new PizzaMenuEntry(52.99, xl, hawaii);
            log.info(PRELOADING + pizzaMenuEntryRepository.save(hawaiiXL));
            PizzaMenuEntry margheritaM = new PizzaMenuEntry(30.99, m, margherita);
            log.info(PRELOADING + pizzaMenuEntryRepository.save(margheritaM));
            PizzaMenuEntry margheritaXL = new PizzaMenuEntry(45.0, xl, margherita);
            log.info(PRELOADING + pizzaMenuEntryRepository.save(margheritaXL));
        };
    }
}
