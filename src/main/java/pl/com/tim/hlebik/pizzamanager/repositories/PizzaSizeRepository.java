package pl.com.tim.hlebik.pizzamanager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.tim.hlebik.pizzamanager.models.PizzaSize;

@Repository
public interface PizzaSizeRepository extends JpaRepository<PizzaSize, Long> {
}
