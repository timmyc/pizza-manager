package pl.com.tim.hlebik.pizzamanager.assemblers;

import org.springframework.stereotype.Component;
import pl.com.tim.hlebik.pizzamanager.models.Pizza;
import pl.com.tim.hlebik.pizzamanager.models.beans.IngredientBean;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaBean;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PizzaAssembler {
    private final IngredientAssembler ingredientAssembler;

    public PizzaAssembler(IngredientAssembler ingredientAssembler) {
        this.ingredientAssembler = ingredientAssembler;
    }

    public PizzaBean mapModelToBean(Pizza pizza) {
        Long id = pizza.getId();
        String name = pizza.getName();
        Set<IngredientBean> ingredients = pizza.getIngredients()
                .stream()
                .map(ingredientAssembler::mapModelToBean)
                .collect(Collectors.toSet());
        return new PizzaBean(id, name, ingredients);
    }
}
