package pl.com.tim.hlebik.pizzamanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaManagerApplication.class, args);
	}

}
