package pl.com.tim.hlebik.pizzamanager.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.com.tim.hlebik.pizzamanager.assemblers.IngredientAssembler;
import pl.com.tim.hlebik.pizzamanager.models.Ingredient;
import pl.com.tim.hlebik.pizzamanager.models.beans.IngredientBean;
import pl.com.tim.hlebik.pizzamanager.services.IngredientService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/ingredient")
public class IngredientRest {
    private final IngredientService ingredientService;
    private final IngredientAssembler ingredientAssembler;

    public IngredientRest(IngredientService ingredientService,
                          IngredientAssembler ingredientAssembler) {
        this.ingredientService = ingredientService;
        this.ingredientAssembler = ingredientAssembler;
    }

    @GetMapping("/all")
    public ResponseEntity<List<IngredientBean>> getAllIngredients() {
        List<IngredientBean> ingredientBeans = ingredientService.getAll()
                .stream()
                .map(ingredientAssembler::mapModelToBean)
                .collect(Collectors.toList());
        return ResponseEntity.ok(ingredientBeans);
    }

    @PostMapping
    public ResponseEntity<IngredientBean> createIngredient(@RequestBody IngredientBean ingredientBean) {
        Ingredient createdIngredient = ingredientService.create(ingredientBean);
        IngredientBean createdIngredientBean = ingredientAssembler.mapModelToBean(createdIngredient);
        return ResponseEntity.ok(createdIngredientBean);
    }

    @PutMapping
    public ResponseEntity<IngredientBean> updateIngredient(@RequestBody IngredientBean ingredientBean) {
        Ingredient updatedIngredient = ingredientService.update(ingredientBean);
        IngredientBean updatedIngredientBean = ingredientAssembler.mapModelToBean(updatedIngredient);
        return ResponseEntity.ok(updatedIngredientBean);
    }

    @GetMapping("/{ingredientId}")
    public ResponseEntity<IngredientBean> getIngredientById(@PathVariable Long ingredientId) {
        Ingredient ingredient = ingredientService.getById(ingredientId);
        IngredientBean ingredientBean = ingredientAssembler.mapModelToBean(ingredient);
        return ResponseEntity.ok(ingredientBean);
    }
}
