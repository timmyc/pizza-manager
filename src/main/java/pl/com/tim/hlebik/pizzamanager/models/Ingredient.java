package pl.com.tim.hlebik.pizzamanager.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Ingredient {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    @Size(min = 3, max = 64)
    private String name;
    @ManyToMany(mappedBy = "ingredients")
    Set<Pizza> pizzas = new HashSet<>();

    public Ingredient(String name) {
        this.name = name;
    }
}
