package pl.com.tim.hlebik.pizzamanager.exceptions;

public class IngredientNotFoundException extends ResourceNotFoundException {
    public IngredientNotFoundException(Long id) {
        super("Could not find ingredient with id: " + id);
    }
}
