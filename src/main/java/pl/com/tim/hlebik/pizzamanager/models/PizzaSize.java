package pl.com.tim.hlebik.pizzamanager.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class PizzaSize {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    @Size(min = 3, max = 64)
    private String name;
    @Positive
    private Integer diameter;
    @OneToMany(mappedBy = "size", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PizzaMenuEntry> pizzaMenuEntries = new HashSet<>();

    public PizzaSize(String name, Integer diameter) {
        this.name = name;
        this.diameter = diameter;
    }
}
