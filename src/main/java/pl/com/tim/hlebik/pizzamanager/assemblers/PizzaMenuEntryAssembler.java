package pl.com.tim.hlebik.pizzamanager.assemblers;

import org.springframework.stereotype.Component;
import pl.com.tim.hlebik.pizzamanager.models.PizzaMenuEntry;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaBean;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaMenuEntryBean;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaSizeBean;

@Component
public class PizzaMenuEntryAssembler {
    private final PizzaAssembler pizzaAssembler;
    private final PizzaSizeAssembler pizzaSizeAssembler;

    public PizzaMenuEntryAssembler(PizzaAssembler pizzaAssembler,
                                   PizzaSizeAssembler pizzaSizeAssembler) {
        this.pizzaAssembler = pizzaAssembler;
        this.pizzaSizeAssembler = pizzaSizeAssembler;
    }

    public PizzaMenuEntryBean mapModelToBean(PizzaMenuEntry pizzaMenuEntry) {
        Long id = pizzaMenuEntry.getId();
        Double price = pizzaMenuEntry.getPrice();
        PizzaSizeBean size = pizzaSizeAssembler.mapModelToBean(pizzaMenuEntry.getSize());
        PizzaBean pizza = pizzaAssembler.mapModelToBean(pizzaMenuEntry.getPizza());

        return new PizzaMenuEntryBean(id, price, size, pizza);
    }
}
