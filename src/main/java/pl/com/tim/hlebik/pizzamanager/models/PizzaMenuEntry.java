package pl.com.tim.hlebik.pizzamanager.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"size_id", "pizza_id"})
})
public class  PizzaMenuEntry {
    @Id
    @GeneratedValue
    private Long id;
    @PositiveOrZero
    private Double price;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "size_id")
    private PizzaSize size;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pizza_id")
    Pizza pizza;

    public PizzaMenuEntry(Double price, PizzaSize size, Pizza pizza) {
        this.price = price;
        this.size = size;
        this.pizza = pizza;
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof PizzaMenuEntry )) return false;
        return id != null && id.equals(((PizzaMenuEntry) object).getId());
    }
}
