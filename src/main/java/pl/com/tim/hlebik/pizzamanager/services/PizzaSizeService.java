package pl.com.tim.hlebik.pizzamanager.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.com.tim.hlebik.pizzamanager.exceptions.PizzaSizeNotFoundException;
import pl.com.tim.hlebik.pizzamanager.models.PizzaSize;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaSizeBean;
import pl.com.tim.hlebik.pizzamanager.repositories.PizzaSizeRepository;

import java.util.List;

@Slf4j
@Service
public class PizzaSizeService {
    private final PizzaSizeRepository pizzaSizeRepository;

    public PizzaSizeService(PizzaSizeRepository pizzaSizeRepository) {
        this.pizzaSizeRepository = pizzaSizeRepository;
    }

    public PizzaSize create(PizzaSizeBean pizzaSizeBean) {
        String name = pizzaSizeBean.getName();
        Integer diameter = pizzaSizeBean.getDiameter();
        PizzaSize pizzaSize = new PizzaSize(name, diameter);
        pizzaSizeRepository.save(pizzaSize);
        log.debug("PizzaSize: " + pizzaSize.getId() + " created successfully");
        return pizzaSize;
    }

    public PizzaSize update(PizzaSizeBean pizzaSizeBean) {
        Long id = pizzaSizeBean.getId();
        String name = pizzaSizeBean.getName();
        Integer diameter = pizzaSizeBean.getDiameter();
        PizzaSize pizzaSize = getById(id);
        pizzaSize.setName(name);
        pizzaSize.setDiameter(diameter);
        pizzaSizeRepository.save(pizzaSize);
        log.debug("Price of pizzaSize: " + pizzaSize.getId() + " updated successfully");
        return pizzaSize;
    }

    public PizzaSize getById(Long id) {
        return pizzaSizeRepository.findById(id).orElseThrow(() -> new PizzaSizeNotFoundException(id));
    }

    public List<PizzaSize> getAll() {
        return pizzaSizeRepository.findAll();
    }


    public Long deleteById(Long id) {
        PizzaSize pizzaSize = getById(id);
        pizzaSizeRepository.delete(pizzaSize);
        log.debug("PizzaSize with id: " + id + " deleted successfully");
        return id;
    }
}
