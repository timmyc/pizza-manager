package pl.com.tim.hlebik.pizzamanager.exceptions;

public class PizzaNotFoundException extends ResourceNotFoundException {
    public PizzaNotFoundException(Long id) {
        super("Could not find pizza with id: " + id);
    }
}
