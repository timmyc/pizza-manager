package pl.com.tim.hlebik.pizzamanager.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.com.tim.hlebik.pizzamanager.assemblers.PizzaAssembler;
import pl.com.tim.hlebik.pizzamanager.models.Pizza;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaBean;
import pl.com.tim.hlebik.pizzamanager.services.PizzaService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/pizza")
public class PizzaRest {
    private final PizzaService pizzaService;
    private final PizzaAssembler pizzaAssembler;

    public PizzaRest(PizzaService pizzaService,
                     PizzaAssembler pizzaAssembler) {
        this.pizzaService = pizzaService;
        this.pizzaAssembler = pizzaAssembler;
    }

    @GetMapping("/all")
    public ResponseEntity<List<PizzaBean>> getAllPizzas() {
        List<PizzaBean> pizzaBeans = pizzaService.getAll()
                .stream()
                .map(pizzaAssembler::mapModelToBean)
                .collect(Collectors.toList());
        return ResponseEntity.ok(pizzaBeans);
    }

    @PostMapping
    public ResponseEntity<PizzaBean> createPizza(@RequestBody PizzaBean pizzaBean) {
        Pizza createdPizza = pizzaService.create(pizzaBean);
        PizzaBean createdPizzaBean = pizzaAssembler.mapModelToBean(createdPizza);
        return ResponseEntity.ok(createdPizzaBean);
    }

    @PutMapping
    public ResponseEntity<PizzaBean> updatePizza(@RequestBody PizzaBean pizzaBean) {
        Pizza updatedPizza = pizzaService.update(pizzaBean);
        PizzaBean updatedPizzaBean = pizzaAssembler.mapModelToBean(updatedPizza);
        return ResponseEntity.ok(updatedPizzaBean);
    }

    @GetMapping("/{pizzaId}")
    public ResponseEntity<PizzaBean> getPizzaById(@PathVariable Long pizzaId) {
        Pizza pizza = pizzaService.getById(pizzaId);
        PizzaBean pizzaBean = pizzaAssembler.mapModelToBean(pizza);
        return ResponseEntity.ok(pizzaBean);
    }

    @DeleteMapping("/{pizzaId}")
    public ResponseEntity<Long> deletePizza(@PathVariable Long pizzaId) {
        Long deletedId = pizzaService.deleteById(pizzaId);
        return ResponseEntity.ok(deletedId);
    }
}
