package pl.com.tim.hlebik.pizzamanager.exceptions;

public class PizzaSizeNotFoundException extends ResourceNotFoundException{
    public PizzaSizeNotFoundException(Long id) {
        super("Could not find pizzaSize with id: " + id);
    }
}
