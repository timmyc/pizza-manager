package pl.com.tim.hlebik.pizzamanager.models.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PizzaSizeBean {
    private Long id;
    private String name;
    private Integer diameter;
}
