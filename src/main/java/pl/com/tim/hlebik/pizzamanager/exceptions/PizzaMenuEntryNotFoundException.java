package pl.com.tim.hlebik.pizzamanager.exceptions;

public class PizzaMenuEntryNotFoundException extends ResourceNotFoundException {
    public PizzaMenuEntryNotFoundException(Long id) {
        super("Could not find pizzaMenuEntry with id: " + id);
    }
}
