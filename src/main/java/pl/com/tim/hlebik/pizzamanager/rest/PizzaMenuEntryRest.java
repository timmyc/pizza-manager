package pl.com.tim.hlebik.pizzamanager.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.com.tim.hlebik.pizzamanager.assemblers.PizzaMenuEntryAssembler;
import pl.com.tim.hlebik.pizzamanager.models.PizzaMenuEntry;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaMenuEntryBean;
import pl.com.tim.hlebik.pizzamanager.services.PizzaMenuEntryService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/pizzaMenuEntry")
public class PizzaMenuEntryRest {
    private final PizzaMenuEntryService pizzaMenuEntryService;
    private final PizzaMenuEntryAssembler pizzaMenuEntryAssembler;

    public PizzaMenuEntryRest(PizzaMenuEntryService pizzaMenuEntryService,
                              PizzaMenuEntryAssembler pizzaMenuEntryAssembler) {
        this.pizzaMenuEntryService = pizzaMenuEntryService;
        this.pizzaMenuEntryAssembler = pizzaMenuEntryAssembler;
    }

    @GetMapping("/all")
    public ResponseEntity<List<PizzaMenuEntryBean>> getAllPizzaMenuEntries() {
        List<PizzaMenuEntryBean> pizzaMenuEntryBeans = pizzaMenuEntryService.getAll()
                .stream()
                .map(pizzaMenuEntryAssembler::mapModelToBean)
                .collect(Collectors.toList());
        return ResponseEntity.ok(pizzaMenuEntryBeans);
    }

    @PostMapping
    public ResponseEntity<PizzaMenuEntryBean> createPizzaMenuEntry(@RequestBody PizzaMenuEntryBean pizzaMenuEntryBean) {
        PizzaMenuEntry createdPizzaMenuEntry = pizzaMenuEntryService.create(pizzaMenuEntryBean);
        PizzaMenuEntryBean createdPizzaMenuEntryBean = pizzaMenuEntryAssembler.mapModelToBean(createdPizzaMenuEntry);
        return ResponseEntity.ok(createdPizzaMenuEntryBean);
    }

    @PutMapping
    public ResponseEntity<PizzaMenuEntryBean> updatePriceOfPizzaMenuEntry(@RequestBody PizzaMenuEntryBean pizzaMenuEntryBean) {
        PizzaMenuEntry updatedPizzaMenuEntry = pizzaMenuEntryService.updatePrice(pizzaMenuEntryBean);
        PizzaMenuEntryBean updatedPizzaMenuEntryBean = pizzaMenuEntryAssembler.mapModelToBean(updatedPizzaMenuEntry);
        return ResponseEntity.ok(updatedPizzaMenuEntryBean);
    }

    @GetMapping("/{pizzaMenuEntryId}")
    public ResponseEntity<PizzaMenuEntryBean> getPizzaMenuEntryById(@PathVariable Long pizzaMenuEntryId) {
        PizzaMenuEntry pizzaMenuEntry = pizzaMenuEntryService.getById(pizzaMenuEntryId);
        PizzaMenuEntryBean pizzaMenuEntryBean = pizzaMenuEntryAssembler.mapModelToBean(pizzaMenuEntry);
        return ResponseEntity.ok(pizzaMenuEntryBean);
    }

    @DeleteMapping("/{pizzaMenuEntryId}")
    public ResponseEntity<Long> deletePizzaMenuEntryById(@PathVariable Long pizzaMenuEntryId) {
        Long deletedId = pizzaMenuEntryService.deleteById(pizzaMenuEntryId);
        return ResponseEntity.ok(deletedId);
    }
}
