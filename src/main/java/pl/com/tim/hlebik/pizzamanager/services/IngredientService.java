package pl.com.tim.hlebik.pizzamanager.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.com.tim.hlebik.pizzamanager.exceptions.IngredientNotFoundException;
import pl.com.tim.hlebik.pizzamanager.models.Ingredient;
import pl.com.tim.hlebik.pizzamanager.models.beans.IngredientBean;
import pl.com.tim.hlebik.pizzamanager.repositories.IngredientRepository;

import java.util.List;

@Slf4j
@Service
public class IngredientService {
    private final IngredientRepository ingredientRepository;

    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public Ingredient create(IngredientBean ingredientBean) {
        String name = ingredientBean.getName();
        Ingredient ingredient = new Ingredient(name);
        ingredientRepository.save(ingredient);
        log.debug("Ingredient: " + name + " created successfully");
        return ingredient;
    }

    public Ingredient update(IngredientBean ingredientBean) {
        Long id = ingredientBean.getId();
        String name = ingredientBean.getName();
        Ingredient ingredient = getById(id);
        ingredient.setName(name);
        ingredientRepository.save(ingredient);
        log.debug("Ingredient: " + name + " updated successfully");
        return ingredient;
    }

    public Ingredient getById(Long id) {
        return ingredientRepository.findById(id).orElseThrow(() -> new IngredientNotFoundException(id));
    }

    public List<Ingredient> getAll() {
        return ingredientRepository.findAll();
    }
}
