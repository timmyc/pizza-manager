package pl.com.tim.hlebik.pizzamanager.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ResourceNotFoundRestAdvice {
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage resourceNotFoundHandler(ResourceNotFoundException notFoundException) {
        int statusCode = HttpStatus.NOT_FOUND.value();
        LocalDateTime now = LocalDateTime.now();
        String exceptionMessage = notFoundException.getMessage();
        return new ErrorMessage(statusCode, now, exceptionMessage);
    }
}
