package pl.com.tim.hlebik.pizzamanager.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.com.tim.hlebik.pizzamanager.exceptions.PizzaNotFoundException;
import pl.com.tim.hlebik.pizzamanager.models.Ingredient;
import pl.com.tim.hlebik.pizzamanager.models.Pizza;
import pl.com.tim.hlebik.pizzamanager.models.beans.IngredientBean;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaBean;
import pl.com.tim.hlebik.pizzamanager.repositories.PizzaRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PizzaService {
    private final PizzaRepository pizzaRepository;
    private final IngredientService ingredientService;

    public PizzaService(PizzaRepository pizzaRepository,
                        IngredientService ingredientService) {
        this.pizzaRepository = pizzaRepository;
        this.ingredientService = ingredientService;
    }

    public Pizza create(PizzaBean pizzaBean) {
        String name = pizzaBean.getName();
        Set<Ingredient> ingredients = pizzaBean.getIngredients()
                .stream()
                .map(IngredientBean::getId)
                .map(ingredientService::getById)
                .collect(Collectors.toSet());
        Pizza pizza = new Pizza(name, ingredients);
        pizzaRepository.save(pizza);
        log.debug("Pizza: " + name + " created successfully");
        return pizza;
    }

    public Pizza update(PizzaBean pizzaBean) {
        Long id = pizzaBean.getId();
        String name = pizzaBean.getName();
        Set<Ingredient> ingredients = pizzaBean.getIngredients()
                .stream()
                .map(IngredientBean::getId)
                .map(ingredientService::getById)
                .collect(Collectors.toSet());
        Pizza pizza = getById(id);
        pizza.setName(name);
        pizza.setIngredients(ingredients);
        pizzaRepository.save(pizza);
        log.debug("Pizza: " + name + " updated successfully");
        return pizza;
    }

    public Pizza getById(Long id) {
        return pizzaRepository.findById(id).orElseThrow(() -> new PizzaNotFoundException(id));
    }

    public List<Pizza> getAll() {
        return pizzaRepository.findAll();
    }

    public Long deleteById(Long id) {
        Pizza pizza = getById(id);
        pizzaRepository.delete(pizza);
        log.debug("Pizza with id: " + id + " deleted successfully");
        return id;
    }
}
