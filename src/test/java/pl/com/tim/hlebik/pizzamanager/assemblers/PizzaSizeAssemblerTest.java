package pl.com.tim.hlebik.pizzamanager.assemblers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import pl.com.tim.hlebik.pizzamanager.models.PizzaSize;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaSizeBean;

@SpringBootTest
class PizzaSizeAssemblerTest {
    @InjectMocks
    private PizzaSizeAssembler pizzaSizeAssembler;

    @Test
    void shouldProperlyMapPizzaMenuEntryModelToBean() {
        String name = "name";
        Integer diameter = 1;
        PizzaSize pizzaMenuEntry = new PizzaSize(name, diameter);
        PizzaSizeBean pizzaMenuEntryBean = pizzaSizeAssembler.mapModelToBean(pizzaMenuEntry);
        Assertions.assertEquals(pizzaMenuEntry.getId(), pizzaMenuEntryBean.getId());
        Assertions.assertEquals(pizzaMenuEntry.getName(), pizzaMenuEntryBean.getName());
        Assertions.assertEquals(pizzaMenuEntry.getDiameter(), pizzaMenuEntryBean.getDiameter());
    }
}
