package pl.com.tim.hlebik.pizzamanager.assemblers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import pl.com.tim.hlebik.pizzamanager.models.Pizza;
import pl.com.tim.hlebik.pizzamanager.models.PizzaMenuEntry;
import pl.com.tim.hlebik.pizzamanager.models.PizzaSize;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaMenuEntryBean;

@SpringBootTest
class PizzaMenuEntryAssemblerTest {
    @Mock
    private PizzaAssembler pizzaAssembler;
    @Mock
    private PizzaSizeAssembler pizzaSizeAssembler;

    @InjectMocks
    private PizzaMenuEntryAssembler pizzaMenuEntryAssembler;

    @Test
    void shouldProperlyMapPizzaMenuEntryModelToBean() {
        Double price = 1d;
        PizzaSize size = Mockito.mock(PizzaSize.class);
        Pizza pizza = Mockito.mock(Pizza.class);
        PizzaMenuEntry pizzaMenuEntry = new PizzaMenuEntry(price, size, pizza);
        PizzaMenuEntryBean pizzaMenuEntryBean = pizzaMenuEntryAssembler.mapModelToBean(pizzaMenuEntry);
        Assertions.assertEquals(pizzaMenuEntry.getId(), pizzaMenuEntryBean.getId());
        Assertions.assertEquals(pizzaMenuEntry.getPrice(), pizzaMenuEntryBean.getPrice());
    }
}
