package pl.com.tim.hlebik.pizzamanager.assemblers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import pl.com.tim.hlebik.pizzamanager.models.Ingredient;
import pl.com.tim.hlebik.pizzamanager.models.Pizza;
import pl.com.tim.hlebik.pizzamanager.models.beans.PizzaBean;

import java.util.Set;

@SpringBootTest
class PizzaAssemblerTest {
    @Mock
    private IngredientAssembler ingredientAssembler;

    @InjectMocks
    private PizzaAssembler pizzaAssembler;

    @Test
    void shouldProperlyMapPizzaModelToBean() {
        String name = "name";
        Ingredient ingredient = Mockito.mock(Ingredient.class);
        Pizza pizza = new Pizza(name, Set.of(ingredient));
        PizzaBean pizzaBean = pizzaAssembler.mapModelToBean(pizza);
        Assertions.assertEquals(pizza.getId(), pizzaBean.getId());
        Assertions.assertEquals(pizza.getName(), pizzaBean.getName());
    }
}
