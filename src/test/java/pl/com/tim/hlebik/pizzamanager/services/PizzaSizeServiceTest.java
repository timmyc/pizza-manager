package pl.com.tim.hlebik.pizzamanager.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import pl.com.tim.hlebik.pizzamanager.exceptions.PizzaSizeNotFoundException;
import pl.com.tim.hlebik.pizzamanager.repositories.PizzaSizeRepository;

import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest
class PizzaSizeServiceTest {

    @Mock
    private PizzaSizeRepository pizzaSizeRepository;

    @InjectMocks
    private PizzaSizeService pizzaSizeService;

    @BeforeEach
    void mockFindById() {
        when(pizzaSizeRepository.findById(1L)).thenReturn(Optional.empty());
    }

    @Test
    void shouldThrowPizzaSizeNotFoundExceptionWhenIncorrectId() {
        Assertions.assertThrowsExactly(PizzaSizeNotFoundException.class,
                () -> pizzaSizeService.getById(1L));
    }
}
