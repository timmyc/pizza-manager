package pl.com.tim.hlebik.pizzamanager.assemblers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import pl.com.tim.hlebik.pizzamanager.models.Ingredient;
import pl.com.tim.hlebik.pizzamanager.models.beans.IngredientBean;

@SpringBootTest
class IngredientAssemblerTest {

    @InjectMocks
    private IngredientAssembler ingredientAssembler;

    @Test
    void shouldProperlyMapIngredientModelToBean() {
        String name = "name";
        Ingredient ingredient = new Ingredient(name);
        IngredientBean ingredientBean = ingredientAssembler.mapModelToBean(ingredient);
        Assertions.assertEquals(ingredient.getName(), ingredientBean.getName());
        Assertions.assertEquals(ingredient.getId(), ingredientBean.getId());
    }
}
