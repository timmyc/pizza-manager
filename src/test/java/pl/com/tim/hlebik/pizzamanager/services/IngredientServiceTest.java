package pl.com.tim.hlebik.pizzamanager.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import pl.com.tim.hlebik.pizzamanager.exceptions.IngredientNotFoundException;
import pl.com.tim.hlebik.pizzamanager.repositories.IngredientRepository;

import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest
class IngredientServiceTest {

    @Mock
    private IngredientRepository ingredientRepository;

    @InjectMocks
    private IngredientService ingredientService;

    @BeforeEach
    void mockFindById() {
        when(ingredientRepository.findById(1L)).thenReturn(Optional.empty());
    }

    @Test
    void shouldThrowIngredientNotFoundExceptionWhenIncorrectId() {
        Assertions.assertThrowsExactly(IngredientNotFoundException.class,
                () -> ingredientService.getById(1L));
    }
}
