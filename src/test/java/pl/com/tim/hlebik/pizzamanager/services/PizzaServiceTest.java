package pl.com.tim.hlebik.pizzamanager.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import pl.com.tim.hlebik.pizzamanager.exceptions.PizzaNotFoundException;
import pl.com.tim.hlebik.pizzamanager.repositories.PizzaRepository;

import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest
class PizzaServiceTest {

    @Mock
    private PizzaRepository pizzaRepository;

    @InjectMocks
    private PizzaService pizzaService;

    @BeforeEach
    void mockFindById() {
        when(pizzaRepository.findById(1L)).thenReturn(Optional.empty());
    }

    @Test
    void shouldThrowPizzaNotFoundExceptionWhenIncorrectId() {
        Assertions.assertThrowsExactly(PizzaNotFoundException.class,
                () -> pizzaService.getById(1L));
    }
}
