package pl.com.tim.hlebik.pizzamanager.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import pl.com.tim.hlebik.pizzamanager.exceptions.PizzaMenuEntryNotFoundException;
import pl.com.tim.hlebik.pizzamanager.repositories.PizzaMenuEntryRepository;

import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest
class PizzaMenuEntryServiceTest {

    @Mock
    private PizzaMenuEntryRepository pizzaMenuEntryRepository;

    @InjectMocks
    private PizzaMenuEntryService pizzaMenuEntryService;

    @BeforeEach
    void mockFindById() {
        when(pizzaMenuEntryRepository.findById(1L)).thenReturn(Optional.empty());
    }

    @Test
    void shouldThrowPizzaMenuEntryNotFoundExceptionWhenIncorrectId() {
        Assertions.assertThrowsExactly(PizzaMenuEntryNotFoundException.class,
                () -> pizzaMenuEntryService.getById(1L));
    }
}
